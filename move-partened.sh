#!/bin/bash
     # usage:
     # $1 input file
     # $2 move-from-path
     # $3 move-to-path
	 
	 
#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	 
     help()
     {
             cat << eof
     Usage: $0 [-n] files...

     -n      do nothing, only see what would be done
     -R      recursive (use find)
     -h      this message
     files   files marked as partened to move

     Examples:
            $0 -n *        (see if everything is ok, then...)
            $0 *

            $0 -R .

eof
     }


     while :
     do
         case "$1" in
             -n)
             apply_cmd='cat'
             shift
             ;;
             -h) help ; exit 1 ;;
             *) break ;;
         esac
     done

     if [ -z "$1" ]; then
             echo Usage: $0 [-h] [-n] [-R] files...
             exit 1
     fi

     for my_line in `sed -n -r '
     # print files with bin portions
     # matches header
     /:/! h

     /:/ {
          x
          G
          # :MARKED: comming from previous cycle => skyp this cycle
          s/MARKED.*/MARKED/
          s/^([^\n]*).*bin.*/\1/p
          s/^([^\n]*)$/MARKED/
          # save current contents
          h
     }
     ' $1`;
     do
          #line_ref=$my_line;
          #indice=`expr index "$my_line" ","`;
          #fileRef=${my_line:0:$indice-1};
          #my_line=${my_line:$indice};
          echo mv $2/$my_line $3;
          mv $2/$my_line $3;
          #echo "line: $line_ref $fileRef, $iniPos, $endPos, $((endPos-iniPos+1))";

     done

     exit 0;

# mv A/f56346296.txt B
# mv A/f8323024.txt B
# mv A/f8650248.txt B
# mv A/f3705176.txt B
