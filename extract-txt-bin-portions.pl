# Lee un archivo mixto UTF-8 - no UTF-8
# output:
# Repeticiones de las l�neas de texto:
# "bin: " byte-inicial-secuencia-no-UTF-8 byte-final-secuencia-no-UTF-8
# "txt: " byte-inicial-secuencia-UTF-8 byte-final-secuencia-UTF-8
# por orden de aparici�n de las secuencias (orden normal de los bytes dentro del archivo)

#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

open my $fh, "<:raw", @ARGV[0] or die "cannot open < @ARGV[0]: $!";
my $i = 0;
my $ini_ciclo_bin = 1;

my $length = read $fh, my $byte, 1;
$i += $length;
# print "a ".unpack("B*", $byte) . "\n";

my $ini_bin;
my $fin_bin;
my $ini_txt;
my $fin_txt;


while($length) {
	# ciclo bin
	while($length and not unpack("B*", $byte) =~ /^(0|110|1110|11110|111110|1111110).*/) {
		$length = read $fh, $byte, 1;
		$i += $length;
		# print "b ".unpack("B*", $byte) . "\n";
	}

	# ciclo txt
	# inicializaci�n secuencia txt
	$ini_txt = $i;
	$fin_txt = 0;	 # no def
	my $s_val = 1;   # boolean TRUE, secuencia txt v�lida (secuencia de caracteres)
	while($length and unpack("B*", $byte) =~ /^(0|110|1110|11110|111110|1111110).*/ and $s_val) {
		# inicializaci�n 1 caracter
		my $l = index(unpack("B*", $byte),'0');
		if($l eq 0) {
			$l = 1;
		}
		my $ini_sec_bytes = $i;

		# lectura 1 caracter
		while($length and $i-$ini_sec_bytes+1 lt $l and $s_val) {
			$length = read $fh, $byte, 1;
			$i += $length;
			# print "c ".unpack("B*", $byte) . "\n";
			if(not unpack("B*", $byte) =~ /^10.*/) {
				$s_val = 0;   # boolean FALSE
			}
		}
		
		if($s_val and $i-$ini_sec_bytes+1 eq $l) {
			$fin_txt = $i;   # def
			$length = read $fh, $byte, 1;
			$i += $length;
			# print "d ".unpack("B*", $byte) . "\n";
		}
	}

	# delimitar secuencias
	if($fin_txt) {   # secuencia txt no vac�a
		if($ini_txt ne $ini_ciclo_bin) {   # secuencia binaria no vac�a
			$ini_bin = $ini_ciclo_bin;
			$fin_bin = $ini_txt - 1;
			print "bin: $ini_bin $fin_bin\n";
		}
		print "txt: $ini_txt $fin_txt\n";
		$ini_ciclo_bin = $fin_txt + 1;   # para la vuelta entrante
	}   # si no las condiciones contin�an igual para reentrar el ciclo-bin
}
# delimitaci�n final
if($i) {   # archivo no vac�o
	if($fin_txt and $fin_txt ne $i) {   # secuencia txt no vac�a ($fin_txt) and quedaron ($i-$fin_txt) bytes bin al final del archivo
		$ini_bin = $fin_txt + 1;
		$fin_bin = $i;
		print "bin: $ini_bin $fin_bin\n";
	}
	elsif($fin_txt eq 0) {   # �ltimo ciclo txt vac�o ==> todo el ciclo corresponde a datos bin
		$ini_bin = $ini_ciclo_bin;
		$fin_bin = $i;
		print "bin: $ini_bin $fin_bin\n";
	}
	# si no si ($fin_txt eq $i) el archivo termina justo en una secuencia txt, �ltimo OUTPUT emitido en ciclo txt
}

close $fh;
