#!/bin/bash

# $1 report file telling the files that match and that don't.
# $2 move from
# $3 move to

#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

for varfile in `cat $1 | sed -n -e s/.doesn.t\ match.\*//p`
do
	#echo "<<$varfile>>"
	mv $2/$varfile $3
done
