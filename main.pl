use Tk;

my $mw = new MainWindow;
my $lSrcDirectory ;
my $bBrowse = $mw->Button(-text=>"Source folder...", -command=> sub{$src= $mw->chooseDirectory(-title=>"Source folder"); $lSrcDirectory->configure(-text=>$src)})->pack();
$lSrcDirectory = $mw->Label()->pack();
my $lDstDirectory;
my $bBrowse2= $mw->Button(-text=>"Destination folder", -command=> sub{$dst=$mw->chooseDirectory(-title=>"Destination folder"); $lDstDirectory->configure(-text=>$dst)})->pack();
$lDstDirectory= $mw->Label()->pack();
my $resume;
my $lResume = $mw->Label()->pack();
sub resume {

        use File::Find;
         $resume='';
        find( \&wanted, ('.') );

        sub wanted {
                # full path in $File::Find::name
                # just filename in $_
                if(/^tokens-processed.txt$/so) {
				   $resume = $_;
				}
				else {
				  
				}
                }

if($resume ne '') {
  $lResume->configure(-text=>"Resume file found: $resume");
}
else {
  $lResume->configure(-text=>"Resume file not found.");
}
}


my $bt2 = $mw->Button(-text=>"Convert", -command=> sub{$myarg_1=$src; $myarg_2=$dst; open SCRIPT, "<", "synopsis.pl"; $script= join "\n", <SCRIPT>; eval $script; resume;})->pack();
use File::Path qw(remove_tree);

my $bDelete = $mw->Button(-text=>"Delete resume file", -command=> sub{remove_tree $resume; resume;})->pack();


resume;
MainLoop;