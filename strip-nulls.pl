# $ARGV[0] : directory where to look for


#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

use open IN  => ":crlf", OUT => ":bytes";
@ARGV = glob("${ARGV[0]}/*");
local $/ = undef;
while(<>) {
	my $content = $_;
	$content =~ s/[\x00-\x1F\x7F]//g;
	if($_ ne $content) {
		print "$ARGV\n";
		$ARGV =~ /(.*?)\.txt/;
		open FILEOUTPUT, ">", "$1-b.txt";
		print FILEOUTPUT $content;
		close $fh;
	}
}
