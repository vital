# Greps txt files to search the string.
# $1: where to look for
# $2: file containing regex


#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.

local $/=undef;
my $fh;
open $fh, '<', ${ARGV[1]};
$my_regex=<$fh>;
$my_regex =~ s/(.)/$1([\\x00-\\x1F]|\\x7F)*/g;
$my_regex =~ s/\(\[\\x00-\\x1F\]\|\\x7F\)\*\?/?/g;

$my_regex =~ s/\n$//g;

print "[Regex]: [$my_regex]\n";

@ARGV = glob "${ARGV[0]}/*";

while(<>) {
	$ARGV =~ s/.*\///;
	if($_ =~ /$my_regex/) {
		print "$ARGV matches (see regex on top of output).-\n";
	}
	else {
		print "$ARGV doesn't match (see regex on top of output).-\n";
	}
}
