#!/bin/bash
     # usage:
     # $1 input file
     # $2 working directory


#    This file is part of TokensV2.
#
#    TokensV2 is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    TokensV2 is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Foobar.  If not, see <http://www.gnu.org/licenses/>.
	 
	 
     help()
     {
             cat << eof
     Usage: $0 [-n] files...

     -n      do nothing, only see what would be done
     -R      recursive (use find)
     -h      this message
     files   files to extract partened

     Examples:
            $0 -n *        (see if everything is ok, then...)
            $0 *

            $0 -R .

eof
     }

     while :
     do
         case "$1" in
             -n)
             apply_cmd='cat'
             shift
             ;;
             -h) help ; exit 1 ;;
             *) break ;;
         esac
     done

     if [ -z "$1" ]; then
             echo Usage: $0 [-h] [-n] [-R] files...
             exit 1
     fi

     for my_line in `sed -n -r '

     # matches header
     /:/! h

     /:/ {
          x
          G
          s/^([^\n]*)\n(txt[^\n]*)\n(bin[^\n]*)$/\1\n\3\n\2/
          s/(.*)\n(.*)\n(.*)\n(.*)/\1\n\3\n\4/
          # save current contents
          h
          s/(.*)\n.*\ntxt: ([0-9]+) ([0-9]+)/\1,\2,\3,/p
     }
     ' $1`;
     do
          #line_ref=$my_line;
          indice=`expr index "$my_line" ","`;
          fileRef=${my_line:0:$indice-1};
          my_line=${my_line:$indice};
          indice=`expr index "$my_line" ","`;
          iniPos=${my_line:0:$indice-1};
          my_line=${my_line:$indice};
          indice=`expr index "$my_line" ","`;
          endPos=${my_line:0:$indice-1};
          my_line=${my_line:$indice};

          #echo "line: $line_ref $fileRef, $iniPos, $endPos, $((endPos-iniPos+1))";

          if [ $((endPos-iniPos+1)) -ge 20 ]
          then
               #echo "File: $fileRef, Start: $iniPos, End: $endPos, Length: $((endPos-iniPos+1)).";
               indext=`expr index "$fileRef" ".txt"`;
               echo dd if=$2/$fileRef of="$2/${fileRef:0:$indext-1}-$iniPos-$endPos.txt" bs=1 skip=$((iniPos-1)) count=$((endPos-iniPos+1)) ;

               dd if=$2/$fileRef of="$2/${fileRef:0:$indext-1}-$iniPos-$endPos.txt" bs=1 skip=$((iniPos-1)) count=$((endPos-iniPos+1)) ;
          fi

     done

     exit 0;

#TXT/a5364798.txt 1 100 $((100-1+1))
#TXT/a5364798.txt 151 1050 $((1050-151+1))
#TXT/a124124.txt 201 340 $((340-201+1))
#TXT/a963455.txt 1 20 $((20-1+1))
#TXT/a963455.txt 151 1000 $((1000-151+1))
#TXT/a963455.txt 1005 1005 $((1005-1005+1))

